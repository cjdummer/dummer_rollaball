﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	public float speed;
    public Text countText;
    public Text winText;
    public Text loseText;
   


    private Rigidbody rb;
    private int count;
    private bool onGround = false;
	
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        loseText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);
        Jump(); //Whenever player presses space, they jump.
    }
	
    void Jump() //controls jumping
    {
        float jumpOffset = 200;
        if (Input.GetKeyDown(KeyCode.Space) && onGround) //Allows the player to jump when they press space
        {
            rb.AddForce(Vector3.up * jumpOffset);
        }
    }
	private void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
        OutOfBounds(); //checks if out of bounds always
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
		rb.AddForce(movement * speed);
	}

    void OutOfBounds() //checks to see if the player has fallen and calls restart level
    {
        if (rb.position.y < -.5)
        {
            RestartLevel();
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }

    void RestartLevel() //once player presses space after this is called, the scene resets
    {
 
        loseText.text = "You Lose! Press Space to Restart";
        if (Input.GetKey(KeyCode.Space))
        {
            loseText.text = "";
            rb.Sleep();
            SceneManager.LoadScene(0);
        }
        
    }
}
