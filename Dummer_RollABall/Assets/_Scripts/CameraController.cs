﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public GameObject player;
	
	Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player.transform.position.y > 0) //controls camera like normal
        {
            transform.position = player.transform.position + offset;
        }
        else //stops the camera from following player down a hole
        {
            transform.position = transform.position;
        }
    }
}
